const express = require('express');
const router = express.Router();

router.get('/:id',(req,res,next) => {
    res.status(200).send(`GET USER ID ${req.params.id}`)
})

router.post('/',(req,res,next) => {
    res.status(201).send('Create new user')
})

module.exports = router
