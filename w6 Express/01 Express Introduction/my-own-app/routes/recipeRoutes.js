const express = require('express');
const router = express.Router();

router.get('/',(req,res,next) => {
    res.status(200).send('GET ALL THE RECIPE')
})

router.get('/:id',(req,res,next) => {
    res.status(200).send(`GET RECIPE ID ${req.params.id}`)
})

router.delete('/:id',(req,res,next) => {
    res.status(204).send(`DELETE RECIPE ID ${req.params.id}`)
})

router.put('/:id',(req,res,next) => {
    res.status(200).send(`MODIFY RECIPE ID ${req.params.id}`)
})

router.post('/',(req,res,next) => {
    res.status(201).send('Create new Recipe')
})

module.exports = router