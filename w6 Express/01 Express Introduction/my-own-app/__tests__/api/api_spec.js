const frisby = require('frisby');

it('should get the recipe ID 1', function () {
  return frisby.get('http://localhost:8080/api/recipe/1')
        .expect('status', 200);
});

it('should get all the recipes', function () {
    return frisby.get('http://localhost:8080/api/recipe')
        .expect('status', 200);
});

it('should delete the recipe ID 2', function () {
    return frisby.delete('http://localhost:8080/api/recipe/2')
        .expect('status', 204);
});

it('should create the new recipe', function () {
    return frisby.post('http://localhost:8080/api/recipe')
        .expect('status', 201);
});

it('should modify the recipe ID 1', function () {
    return frisby.put('http://localhost:8080/api/recipe/1')
        .expect('status', 200);
});

it('should create the new user', function () {
    return frisby.post('http://localhost:8080/api/user')
        .expect('status', 201);
});

it('should get all the user ID 3', function () {
    return frisby.get('http://localhost:8080/api/user/3')
        .expect('status', 200);
});