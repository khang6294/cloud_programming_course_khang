const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const path = require('path')
const compression = require('compression')
const api = require('./api')
const port = process.env.PORT || 8080;


app.use(bodyParser.json()); // application/json
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/api',api)

app.listen(port)