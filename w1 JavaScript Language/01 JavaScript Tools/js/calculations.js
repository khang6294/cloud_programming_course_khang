const calForm = document.getElementById("calculation-form");
const firstNum = document.getElementById("firstNumber");
const secondNum = document.getElementById("secondNumber");
const result = document.getElementById("result");
const firstErr = document.getElementById("firstErr");
const secondErr = document.getElementById("secondErr");


firstNum.addEventListener('keyup' , (e) => {
    const value = event.target.value
    if (isNaN(value)) {
        firstErr.innerText = "Please input a number";
    } else {
        firstErr.innerText = "";
    }
})


secondNum.addEventListener('keyup' , (e) => {
    const value = event.target.value
    if (isNaN(value)) {
        secondErr.innerText = "Please input a number";
    } else {
        secondErr.innerText = "";
    }
})


const formValid = () => {
    return firstNum.value && secondNum.value
}

function multiplyBy() {
    if (formValid()) {
        result.innerText = firstNum.value * secondNum.value;
    } else {
        result.innerText = "Input cannot be empty!";
    }
}

function divideBy() { 
    if (formValid()) {
        if (secondNum.value === "0") {
            secondErr.innerText = "Invalid to divide by 0";
            result.innerText = "";
        } else {
            result.innerText = firstNum.value / secondNum.value;    
        }
    } else {
        result.innerText = "Input cannot be empty!";
    }
}