'use strict'

let targetNumber = Math.floor(Math.random() * 10) + 1;
let endGame = false;
let turn = 0;
const numberInput = document.querySelector("input[name='number']");
const submitBtn = document.querySelector("#submit-btn");

function init () {
	submitBtn.addEventListener('click', e => {
		e.preventDefault();
		if(!endGame) {
			check(numberInput.value)
		}
	});
}

function check (value) {
	turn+=1;
	if (parseInt(value) === targetNumber) {
		showWin();
	} else if (turn >= 5) {
		showLoss();
	} else {
		showError();
	}
}

function showWin () {
	console.log(`Congrats! You win. Answer is ${targetNumber}`);
	endGame = true;
}

function showError () {
	console.log(`Wrong. You have ${5 - turn} turns left`);
}

function showLoss () {
	console.log(`Oops, you lose! Answer is ${targetNumber}`);
	endGame = true;
}

init();